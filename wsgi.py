#! env python3

import argparse
import logging

from src.api import app

if __name__ == '__main__':
    logging_levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }

    parser = argparse.ArgumentParser(description='Launch flask app')
    parser.add_argument(
        '-d',
        '--debug',
        help='Enable debug mode',
        action='store_true'
    )
    parser.add_argument(
        '-L',
        '--logging',
        help='Logging level',
        default='info',
        choices=logging_levels.keys()
    )

    args = parser.parse_args()

    app.logger.setLevel(logging_levels[args.logging])
    app.run(debug=args.debug)
