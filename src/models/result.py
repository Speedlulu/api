from ..utils.db import db


__all__ = ('Result', )


class Result(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    output = db.Column(db.Text, nullable=True)
    dequeued_on = db.Column(db.DateTime, nullable=False)
    finished_on = db.Column(db.DateTime, server_default=db.func.now())
    code = db.Column(db.Integer, nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'output': self.output,
            'dequeued_on': self.dequeued_on,
            'finished_on': self.finished_on,
            'code': self.code,
        }
