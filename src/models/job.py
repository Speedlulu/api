from enum import IntEnum

from ..utils.db import db


__all__ = ('Job', 'JobState', )


class JobState(IntEnum):
    WAITING = 0
    DONE = 1
    ERROR = 2


class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    command = db.Column(db.Text, nullable=False)
    state = db.Column(db.Enum(JobState), nullable=False)
    queued_on = db.Column(db.DateTime, server_default=db.func.now())
    asker = db.Column(db.String(256), nullable=False)
    result_id = db.Column(
        db.Integer,
        db.ForeignKey('result.id'),
        nullable=True
    )
    result = db.relationship('Result', backref=db.backref('job'))

    def to_dict(self):
        return {
            'id': self.id,
            'command': self.command,
            'state': self.state,
            'queued_on': self.queued_on,
            'asker': self.asker,
            'result_id': self.result_id,
        }
