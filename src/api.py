from flask import (
    abort, jsonify, make_response, request,
)

from .blueprints import (
    command, enroll, info, jobs, mc_monitor, notif,
)
from .security.request_auth import is_authorized
from .utils.custom_classes import PublicFlask
from .utils.config import config
from .utils.db import db
from .utils.response import make_response as c_make_response


app = PublicFlask(__name__)


app.secret_key = config.app_secret_key.encode('utf-8')
app.config['SQLALCHEMY_DATABASE_URI'] = config.app_sqlalchemy_database_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db.init_app(app)
app.config['db'] = db

if config.app_log_level is not None:
    app.logger.setLevel(config.app_log_level)


@app.errorhandler(400)
def fourOO_handler(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            400
        )
    )


@app.errorhandler(401)
def fourOone_handler(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            401
        )
    )


@app.errorhandler(404)
def fourOfour_hanlder(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            404
        )
    )


@app.errorhandler(405)
def fourOfive_handler(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            405
        )
    )


@app.errorhandler(409)
def fourOnine_handler(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            409
        )
    )


@app.errorhandler(500)
def fiveOO_handler(error):
    return make_response(
        (
            jsonify({'Error': 'Internal Error'}),
            500
        )
    )


@app.errorhandler(504)
def fiveOfour_handler(error):
    return make_response(
        (
            jsonify({'Error': error.description}),
            504
        )
    )


@app.before_request
def auth_handler():
    if request.path in app.public_routes:
        pass
    elif not is_authorized(request):
        abort(401)


@app.route('/')
def home():
    return c_make_response('Hello')


@app.route('/routes')
def get_routes():
    route_dict = {}
    for rule in app.url_map.iter_rules():
        if rule.rule in app.route_args.keys():
            if not route_dict.get(rule.rule, None):
                methods = list(rule.methods)
            else:
                methods = list(set(
                    route_dict[rule.rule]['methods']
                ).union(rule.methods))
            route_dict[rule.rule] = {
                'args': {
                    key: (value if not callable(value) else value())
                    for key, value in app.route_args[rule.rule].items()
                },
                'methods': methods,
            }
        else:
            route_dict[rule.rule] = {'methods': list(rule.methods)}
    return c_make_response(route_dict)


app.register_blueprint(notif.bp)
app.register_blueprint(info.bp)
app.register_blueprint(command.bp)
app.register_blueprint(enroll.bp)
app.register_blueprint(mc_monitor.bp)
app.register_blueprint(jobs.bp)
