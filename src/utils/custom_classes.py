import re

from flask import Flask, Blueprint


class ArgumentError(Exception):
    pass


class PublicFlask(Flask):
    def __init__(self, import_name, **kwargs):
        super().__init__(import_name, **kwargs)
        self.public_routes = []
        self.route_args = {}

    def register_blueprint(self, blueprint, **options):
        self.public_routes.extend(blueprint.public_routes)
        self.route_args.update(blueprint.route_args)
        super().register_blueprint(blueprint, **options)

    def public_route(self, rule, **options):
        def decorator(f):
            self.public_routes.append(rule)
            endpoint = options.pop('endpoint', None)
            route_args = options.pop('route_args', None)
            if route_args:
                for key in route_args.keys():
                    if not re.search(r'<(.+:)?%s>' % key, rule):
                        raise ArgumentError(
                            'Bad argument %s for rule %s' % (key, rule)
                        )
                self.route_args[rule] = route_args
            self.add_url_rule(rule, endpoint, f, **options)
            return f
        return decorator

    def route(self, rule, **options):
        def decorator(f):
            endpoint = options.pop('endpoint', None)
            route_args = options.pop('route_args', None)
            if route_args:
                for key in route_args.keys():
                    if not re.search(r'<(.+:)?%s>' % key, rule):
                        raise ArgumentError(
                            'Bad argument %s for rule %s' % (key, rule)
                        )
                self.route_args[rule] = route_args
            self.add_url_rule(rule, endpoint, f, **options)
            return f
        return decorator


class PublicBlueprint(Blueprint):
    def __init__(self, name, import_name, **kwargs):
        super().__init__(name, import_name, **kwargs)
        self.public_routes = []
        self.route_args = {}

    def public_route(self, rule, **options):
        def decorator(f):
            self.public_routes.append(self.url_prefix + rule)
            route_args = options.pop('route_args', None)
            if route_args:
                for key in route_args.keys():
                    if not re.search(r'<(.+:)?%s>' % key, rule):
                        raise ArgumentError(
                            'Bad argument %s for rule %s' % (
                                key,
                                self.url_prefix + rule
                            )
                        )
                self.route_args[self.url_prefix + rule] = route_args
            endpoint = options.pop("endpoint", f.__name__)
            self.add_url_rule(rule, endpoint, f, **options)
            return f
        return decorator

    def route(self, rule, **options):
        def decorator(f):
            endpoint = options.pop("endpoint", f.__name__)
            route_args = options.pop('route_args', None)
            if route_args:
                for key in route_args.keys():
                    if not re.search(r'<(.+:)?%s>' % key, rule):
                        raise ArgumentError(
                            'Bad argument %s for rule %s' % (
                                key,
                                self.url_prefix + rule
                            )
                        )
                self.route_args[self.url_prefix + rule] = route_args
            self.add_url_rule(rule, endpoint, f, **options)
            return f
        return decorator
