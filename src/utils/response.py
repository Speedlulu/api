from flask import jsonify, make_response as f_make_response


def make_response(*args):
    return f_make_response(jsonify({'content': args[0]}), *args[1:])
