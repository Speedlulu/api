#!/usr/bin/python3

from configparser import ConfigParser
from pathlib import Path
from shutil import copyfile
from subprocess import check_output


__all__ = ('config', )


class Config:
    def __init__(self, config_entries):
        self.config_entries = config_entries

    def __getattr__(self, name):
        value = self.config_entries.get(name)
        if value is None:
            pass
        elif value.startswith('`') and value.endswith('`'):
            value = check_output(
                value[1:-1],
                shell=True
            ).decode('utf-8').rstrip('\n')
        # integer
        elif value.isnumeric():
            value = self.config_entries.getint(name)
        # boolean
        elif value in ConfigParser.BOOLEAN_STATES:
            value = self.config_entries.getboolean(name)
        # none of the above
        setattr(self, name, value)
        return value


config_path = Path('config.ini')
if not config_path.exists():
    example_config_path = Path('config_example.ini')
    copyfile(str(example_config_path.absolute()), str(config_path.absolute()))

config_parser = ConfigParser()
config_parser.read(str(config_path.absolute()))
config = Config(config_parser['config'])
