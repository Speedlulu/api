import subprocess


def get_output(command, args=''):
    output = subprocess.check_output(
        '%s %s' % (command, args),
        shell=True
    ).decode('utf-8')

    if output[-1] == '\n':
        output = output[:-1]
    return output
