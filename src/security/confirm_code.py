import random
import string
import tempfile
import uuid

from flask import (
    request, abort, session, make_response,
    jsonify
)

from ..blueprints.notif import notif_request


CODE_FILE = '/etc/api/validation_codes'


def generate_code(length):
    return ''.join(random.choice(string.digits) for i in range(length))


def write_code(code_id, code):
    with open(CODE_FILE, 'a') as f:
        f.write('%s:%s\n' % (code_id, code))


def get_code(code_id):
    with tempfile.TemporaryFile(mode='r+') as ftemp:
        with open(CODE_FILE, 'r+') as f:
            for line in f.readlines():
                if line.split(':')[0]:
                    code = line.split(':')[1].strip('\n')
                else:
                    ftemp.write(line)
            else:
                f.seek(0)
                f.truncate()
                ftemp.seek(0)
                for tline in ftemp.readlines():
                    f.write(tline)
    return code


def confirm_code(func):
    def wrapper():
        if not request.headers.get('X-Confirmation-Code'):
            code = generate_code(4)
            code_id = uuid.uuid4().hex

            write_code(code_id, code)

            session['code_id'] = code_id
            # Implement something to handle an error on notif request
            notif_request('Confimation code', code)

            return make_response(jsonify(
                {
                    'confirmation': {
                        'value': 'Confirmation code needed'
                    }
                }
            ))
        code = get_code(session['code_id'])
        if code == request.headers['X-Confirmation-Code']:
            return func()
        else:
            abort(401, 'Wrong code')
    return wrapper
