import configparser

from oauth2client.service_account import ServiceAccountCredentials

SCOPES = ['https://www.googleapis.com/auth/firebase.messaging']
CREDENTIAL_FILE = '/etc/api/serverinterface-ed604-firebase-adminsdk-d1lwa-03b9e3cfb8.json'
SMS_API_CONFIG = '/etc/api/sms_api.ini'


def get_access_token():
    """Retrieve a valid access token that can be used to authorize requests.
    :return: Access token.
    """
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        CREDENTIAL_FILE,
        SCOPES
    )
    access_token_info = credentials.get_access_token()
    return access_token_info.access_token


def get_sms_api_config():
    """
    Retrieve sms api config
    """
    parser = configparser.ConfigParser()
    parser.read(SMS_API_CONFIG)
    return {
        'user': parser['config']['user'],
        'pass': parser['config']['pass'],
    }
