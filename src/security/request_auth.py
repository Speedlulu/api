from base64 import b64decode

from Cryptodome.PublicKey import RSA
from Cryptodome.Signature import PKCS1_v1_5
from Cryptodome.Hash import SHA256
from flask import g


AUTHORIZED_KEYS = '/etc/api/authorized_keys'


def is_authorized(r):
    signed_string = r.method.upper() + ' ' + r.url
    if signed_string[-1] == '/':
        signed_string = signed_string[:-1]
    hashed_string = SHA256.new(signed_string.encode('utf-8'))
    api_signature = r.headers.get('X-API-Signature')
    if not api_signature:
        return False
    signature = b64decode(api_signature)
    for key in open(AUTHORIZED_KEYS).read().split('\n')[:-1]:
        pub_key = RSA.importKey(key)
        if PKCS1_v1_5.new(pub_key).verify(hashed_string, signature):
            g.identity = key.rsplit(maxsplit=1)[-1]
            return True
    return False
