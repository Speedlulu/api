from Cryptodome.PublicKey import RSA
from flask import abort, request

from .notif import new_notif
from ..utils.custom_classes import PublicBlueprint
from ..utils.response import make_response


bp = PublicBlueprint('enroll', __name__, url_prefix='/enroll')


@bp.public_route('', methods=('POST',))
def enroll_key():
    if not request.form.get('key'):
        abort(400, 'No key provided')
    if not request.form.get('identity'):
        abort(400, 'No identity given')
    identity = request.form.get('identity')
    key_string = request.form.get('key')

    try:
        RSA.importKey(key_string)
    except ValueError:
        abort(400, 'Wrong key format, provide RSA key under the PEM format')

    with open('enroll/%s_pubkey' % identity, 'w') as f:
        f.write(key_string)

    new_notif('New enrollment request', 'Key submited by %s' % identity)

    return make_response('Key added to list of queries')
