import ast

from flask import (
    abort, request,
)

from ..utils.customparser import parser, create_config_dict
from ..utils.custom_classes import PublicBlueprint
from ..utils.response import make_response


CONFIG_FILE = 'src/static/events_notif.cfg'
LOGS_FILE = 'src/static/old_logs'

bp = PublicBlueprint('mc_monitor', __name__, url_prefix='/mc_monitor')


@bp.route('/config', methods=('GET',))
def get_config():
    parser.clear()
    parser.read(CONFIG_FILE)
    return make_response(create_config_dict(parser))


@bp.route('/logs', methods=('GET',))
def get_logs():
    with open(LOGS_FILE) as f:
        contents = f.read()
    if contents[-1] == '\n':
        contents = contents[:-1]
    return make_response(contents.split('\n'))


def get_events():
    parser.clear()
    parser.read(CONFIG_FILE)
    return parser.sections()


@bp.route(
    '/config/<event>',
    methods=('PATCH',),
    route_args={'event': get_events}
)
def patch_event(event):
    parser.clear()
    parser.read(CONFIG_FILE)
    if event not in parser.sections():
        abort(400, 'No such event : %s' % event)
    config = create_config_dict(parser)
    for key, value in request.form.items():
        if key in config[event].keys():
            config[event][key] = ast.literal_eval(value)
        else:
            abort(400, 'Wrong key %s for event %s' % (key, event))
    parser.clear()
    parser.read_dict(config)
    with open(CONFIG_FILE, 'w') as f:
        parser.write(f)

    return make_response(config[event])


@bp.route(
    '/config/<event>',
    methods=('GET',),
    route_args={'event': get_events}
)
def get_event(event):
    parser.clear()
    parser.read(CONFIG_FILE)
    if event not in parser.sections():
        abort(400, 'No such event : %s' % event)
    return make_response(create_config_dict(parser).get(event))
