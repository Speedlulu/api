from flask import abort, request

from ..utils.command import get_output
from ..utils.custom_classes import PublicBlueprint
from ..utils.response import make_response


bp = PublicBlueprint('info', __name__, url_prefix='/info')


class Resource():
    def __init__(self, name, value, info={}):
        self.name = name
        self.value = value
        self.info = info


def get_all_resources(data):
    resource_list = list()

    # This whole mess is so that you can specifiy data
    # to send to specific functions in post data
    # Under the format <function>_<data_name>
    # Mainly used to send args
    # Example {'uname_args': '-r'}
    for key, value in RESOURCE_FUNCTIONS.items():
        func_data = dict()
        for data_key, data_value in data.items():
            if data_key.startswith(key + '_'):
                func_data_key = data_key.split(key + '_')[-1]
                if func_data_key in func_data.keys():
                    func_data[func_data_key] += data_value
                else:
                    func_data[func_data_key] = data_value
            elif not any(d_key.startswith(key + '_') for d_key in data.keys()):
                if data_key in func_data.keys():
                    func_data[data_key] += data_value
                else:
                    func_data[data_key] = data_value
        if key != 'all':
            resource_list += value(func_data)
    return resource_list


def get_cpu_usage(data):
    DEFAULT_ARGS = '-P ALL'
    args = data.get('args', DEFAULT_ARGS)
    output = [
        line.split()[1:] for line in get_output(
            'mpstat', args=args
        ).split('\n')[2:]
    ]

    header = output[0]
    info = output[1:]

    cpu_dict = dict()

    for line in info:
        current_dict = dict()
        for index, value in enumerate(line[1:]):
            current_dict[header[index + 1]] = value
        cpu_dict[line[0]] = current_dict

    return [vars(Resource('cpu_usage', cpu_dict))]


def get_memory_usage(data):
    DEFAULT_ARGS = '-m'
    args = data.get('args', DEFAULT_ARGS)
    output = [
        line.split() for line in get_output(
            'free', args=args
        ).split('\n')[:2]
    ]
    header = output[0]
    info = output[1][1:]
    memory_dict = dict(zip(header, info))
    return [vars(Resource('memory_usage', memory_dict))]


def get_disk_usage(data):
    DEFAULT_ARGS = '-H'
    args = data.get('args', DEFAULT_ARGS)
    output = get_output('df', args=args).split('\n')
    header = output[0][:-1].split()
    info = [line.split() for line in output[1:]]
    disk_usage_dict = dict()

    for line in info:
        current_dict = dict()
        for index, value in enumerate(line[:-1]):
            current_dict[header[index]] = value
        disk_usage_dict[line[-1]] = current_dict

    return [vars(Resource('disk_usage', disk_usage_dict))]


def get_processes(data):
    return [vars(Resource('processes', '4'))]


def get_uname(data):
    DEFAULT_ARGS = '-a'
    args = data.get('args', DEFAULT_ARGS)
    value = get_output('uname', args=args)
    return [vars(Resource('uname', value))]


RESOURCE_FUNCTIONS = {
    'all': get_all_resources,
    'cpu_usage': get_cpu_usage,
    'memory_usage': get_memory_usage,
    'disk_usage': get_disk_usage,
    'processes': get_processes,
    'uname': get_uname,
}


@bp.route(
    '/server/<resource>',
    methods=('GET', 'POST',),
    route_args={
        'resource': [key for key in RESOURCE_FUNCTIONS.keys()]
    }
)
def server_info(resource):
    func = RESOURCE_FUNCTIONS.get(resource)
    if func:
        return make_response({'resources': func(request.form)})
    abort(404, 'Could not find the desired resource')


@bp.route('/server', methods=('GET', 'POST',))
def all_server_info():
    return server_info('all')
