import json

import requests
from flask import make_response, jsonify, abort, request, current_app
from httplib2 import ServerNotFoundError
from requests.exceptions import ConnectionError

from ..security.token import get_access_token, get_sms_api_config
from ..utils.custom_classes import PublicBlueprint


bp = PublicBlueprint('notif', __name__, url_prefix='/notif')


def log_firebase_message_response(func):
    def wrapper(*args, **kwargs):
        try:
            response = func(*args, **kwargs)
        except (ConnectionError, ServerNotFoundError):
            current_app.logger.error(
                'Could not connect to notification server'
            )
        else:
            if response.ok:
                current_app.logger.info(
                    'Successful request for notication : %s' %
                    json.loads(response.text)['name'].split('/')[-1]
                )
            else:
                current_app.logger.error(
                    'Error during request for notification'
                )
            return response
    return wrapper


# Implement more than logging on error, retry for example
@log_firebase_message_response
def notif_request(title, body='', topic='all'):
    headers = {
        'Authorization': 'Bearer %s' % get_access_token(),
        'Content-Type': 'application/json',
    }

    data = json.dumps({
        'message': {
            'topic': topic,
            'notification': {
                'body': body,
                'title': title
            }
        }
    })

    url = 'https://fcm.googleapis.com/v1/projects/serverinterface-ed604/messages:send'

    current_app.logger.info('Making request for notfication')

    return requests.post(url=url, data=data, headers=headers)


@bp.route('/new', methods=('POST',))
def new_notif(title=None, body=None):
    if (
        not request.form.get('title') or not request.form.get('body')
    ) and (not title or not body):
        abort(400, 'No title or body specified')
    response = notif_request(
        request.form.get('title', title),
        request.form.get('body', body)
    )
    if response is None:
        abort(504, 'Could not contact notification server')
    return make_response(
        jsonify(
            {
                'response_firebase': {
                    'text': response.text,
                    'status_code': response.status_code
                }
            }
        )
    )


@bp.route('/test', methods=('GET',))
def test_notif():
    response = notif_request('Test', 'test')
    if response is None:
        abort(504, 'Could not contact notification server')
    return make_response(
        jsonify(
            {
                'response_firebase': {
                    'text': response.text,
                    'status_code': response.status_code
                }
            }
        )
    )


@bp.route('/sms/new', methods=('POST',))
def new_sms(content=None):
    if request.form.get('content') is None and content is None:
        abort(400, 'No title or body specified')
    content = request.form.get('content', content)
    response = requests.post(
        'https://smsapi.free-mobile.fr/sendmsg',
        json={**get_sms_api_config(), 'msg': content},
    )
    response_content = response.content
    if not response_content:
        if response.status_code == 200:
            response_content = 'Success'  
        elif response.status_code == 400:
            response_content = 'Missing parameter'
        elif response.status_code == 402:
            response_content = 'Too many messages sent'
        elif response.status_code == 403:
            response_content = 'Access denied'
        elif response.status_code == 500:
            response_content = 'Server error'
    return make_response(
        jsonify({'api_response': response_content}),
        response.status_code,
    )
