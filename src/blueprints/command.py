from subprocess import CalledProcessError

from flask import (
    request, abort, current_app
)

from ..security.confirm_code import confirm_code
from ..utils.command import get_output
from ..utils.custom_classes import PublicBlueprint
from ..utils.response import make_response

bp = PublicBlueprint('command', __name__, url_prefix='/command')


class Command():
    def __init__(self, command, args, output):
        self.command = command
        self.args = args
        self.output = output


@bp.route('', methods=('POST',))
@confirm_code
def exec_command():
    if not request.form.get('command'):
        abort(400, 'No command provided')
    try:
        command_output = get_output(
            request.form['command'], args=request.form.get('args', '')
        )
    except FileNotFoundError:
        abort(400, 'No such command')
    except OSError:
        abort(400, 'Invalid string provided')
    except CalledProcessError as e:
        abort(400, 'Command returned non-zero exit code %d : %s' % (
            e.returncode,
            e.output.decode('utf-8')
        ))
    current_app.logger.info('Executed command %s %s' % (
        request.form['command'],
        request.form.get('args', '')
    ))
    return make_response(
        vars(
            Command(
                request.form['command'],
                request.form.get('args', ''),
                command_output
            )
        )
    )
