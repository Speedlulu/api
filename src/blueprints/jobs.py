from configparser import ConfigParser
from datetime import datetime, timedelta
from pathlib import Path

from flask import abort, request, current_app, g
from sqlalchemy.orm.exc import NoResultFound

from ..blueprints.notif import notif_request
from ..models import *
from ..security.confirm_code import confirm_code
from ..utils.config import config
from ..utils.custom_classes import PublicBlueprint
from ..utils.response import make_response


bp = PublicBlueprint('job', __name__, url_prefix='/job')


@bp.route('/all', methods=('GET', ))
def get_all_jobs():
    return make_response(
        [
            job.to_dict()
            for job in Job.query.order_by(Job.queued_on).all()
        ]
    )


@bp.route('/queued', methods=('GET', ))
def get_queued_jobs():

    jobs = Job.query.filter(
        Job.state == JobState.WAITING
    ).order_by(Job.queued_on).all()

    if request.headers.get('X-Polling-Client') is not None:
        _do_client_polling_stats(g.identity)

    return make_response([job.to_dict() for job in jobs])


@bp.route('/executed', methods=('GET', ))
def get_executed_jobs():
    if 'done' in request.args:
        response = make_response(
            [
                job.to_dict()
                for job in Job.query.filter(
                    Job.state == JobState.DONE
                ).order_by(Job.queued_on).all()
            ]
        )
    elif 'error' in request.args:
        response = make_response(
            [
                job.to_dict()
                for job in Job.query.filter(
                    Job.state == JobState.ERROR
                ).order_by(Job.queued_on).all()
            ]
        )
    else:
        response = make_response(
            [
                job.to_dict()
                for job in Job.query.filter(
                    Job.state != JobState.WAITING
                ).order_by(Job.queued_on).all()
            ]
        )

    return response


@bp.route('/<int:_id>', methods=('GET', ))
def get_job(_id):
    try:
        job = Job.query.filter(Job.id == _id).one()
    except NoResultFound:
        abort(404, 'No such job')
    return make_response(
        job.to_dict()
    )


@bp.route('', methods=('POST', ))
@confirm_code
def queue_job():

    command = request.form.get('command')
    if command is None:
        abort(400, 'Command argument needed')

    db = current_app.config['db']

    new_job = Job()
    new_job.command = command
    new_job.state = JobState.WAITING
    new_job.asker = g.identity

    db.session.add(new_job)
    db.session.commit()

    client_stats = get_client_stats()
    if client_stats is not None:
        notif_request(
            'Job %d queued' % new_job.id,
            'Average polling frequency: %s\nLast poll: %s' % (
                client_stats['avg_polling_frequency'],
                client_stats['last_poll'],
            )
        )
    else:
        notif_request('Job %d queued' % new_job.id)

    return make_response(new_job.to_dict())


@bp.route('/<int:_id>', methods=('PUT',))
def close_job(_id):

    db = current_app.config['db']

    try:
        job = Job.query.filter(Job.id == _id).one()
    except NoResultFound:
        abort(404, 'No such job')

    if job.state != JobState.WAITING:
        abort(409, 'Job already closed')

    output = request.form.get('output')
    dequeued_on = request.form.get('dequeued_on')
    finished_on = request.form.get('finished_on')
    code = request.form.get('code')

    if not all(
        (_ is not None for _ in (output, dequeued_on, finished_on, code))
    ):
        abort(400, 'Missing argument')

    job_result = Result(
        output=output,
        dequeued_on=datetime.fromtimestamp(int(dequeued_on)),
        finished_on=datetime.fromtimestamp(int(finished_on)),
        code=int(code)
    )

    db.session.add(job_result)
    db.session.flush()

    if job_result.code != 0:
        job.state = JobState.ERROR
    else:
        job.state = JobState.DONE

    job.result_id = job_result.id
    job.result = job_result

    db.session.commit()

    if request.headers.get('X-Polling-Client') is not None:
        _do_client_executed_stats(g.identity)

    notif_request(
        '%s' % (
            'Job %d executed'
            if job.state == JobState.DONE
            else 'Error on job %d'
        ) % job.id,
        'Output: %s\n=====\nCode:%d' % (job_result.output, job_result.code)
    )

    return make_response(job.to_dict())


@bp.route('/<int:_id>/result', methods=('GET', ))
def get_job_result(_id):
    try:
        job = Job.query.filter(Job.id == _id).one()
    except NoResultFound:
        abort(404, 'No such job')

    if job.result is None:
        abort(404, 'No result for this job, probably not yet executed')

    return make_response(job.result.to_dict())


CONVERTERS = {
    'datetime': lambda x: datetime.fromtimestamp(int(x)),
    'timedelta': lambda x: timedelta(seconds=int(x))
}


def _do_client_executed_stats(identity):
    info_path = Path(config.app_client_info)

    client_info = ConfigParser(converters=CONVERTERS)

    if not info_path.exists():
        client_info['CLIENT'] = {
            'nb_poll': 1,
            'identity': identity,
            'avg_polling_frequency': 0,
            'last_poll': int(datetime.now().timestamp()),
            'nb_job_executed': 1,
        }
        current_app.logger.info('Client info file not found, creating it')
    else:
        with info_path.open('r') as info_file:
            client_info.read_file(info_file)

        client_info['CLIENT']['nb_job_executed'] = str(
            client_info['CLIENT'].getint('nb_job_executed') + 1
        )

    with info_path.open('w') as info_file:
        client_info.write(info_file)

    current_app.logger.info('Updated client info')


def _do_client_polling_stats(identity):
    info_path = Path(config.app_client_info)

    client_info = ConfigParser(converters=CONVERTERS)

    if not info_path.exists():
        client_info['CLIENT'] = {
            'nb_poll': 1,
            'identity': identity,
            'avg_polling_frequency': 0,
            'last_poll': int(datetime.now().timestamp()),
            'nb_job_executed': 0,
        }
        current_app.logger.info('Client info file not found, creating it')
    else:
        with info_path.open('r') as info_file:
            client_info.read_file(info_file)

        now = datetime.now()
        delta = now - client_info['CLIENT'].getdatetime('last_poll')
        nb_poll = client_info['CLIENT'].getint('nb_poll')
        file_avg_polling_frequency = client_info['CLIENT'].gettimedelta(
            'avg_polling_frequency'
        )

        if file_avg_polling_frequency.total_seconds() == 0:
            client_info['CLIENT']['avg_polling_frequency'] = str(
                int(delta.total_seconds())
            )
        else:
            new_avg_delta = (
                delta + (file_avg_polling_frequency * nb_poll)
            ) / (nb_poll + 1)
            client_info['CLIENT']['avg_polling_frequency'] = str(
                int(new_avg_delta.total_seconds())
            )

        client_info['CLIENT']['last_poll'] = str(int(now.timestamp()))
        client_info['CLIENT']['nb_poll'] = str(nb_poll + 1)

    with info_path.open('w') as info_file:
        client_info.write(info_file)

    current_app.logger.info('Updated client info')


def get_client_stats():
    info_path = Path(config.app_client_info)

    if not info_path.exists():
        return None

    client_info = ConfigParser(converters=CONVERTERS)

    with info_path.open('r') as info_file:
        client_info.read_file(info_file)

    return {
        'identity': client_info['CLIENT'].get('identity'),
        'nb_poll': client_info['CLIENT'].getint('nb_poll'),
        'avg_polling_frequency': str(
            client_info['CLIENT'].gettimedelta('avg_polling_frequency')
        ),
        'last_poll': client_info['CLIENT'].getdatetime('last_poll'),
        'nb_job_executed': client_info['CLIENT'].getint('nb_job_executed'),
    }


@bp.route('/client', methods=('GET', ))
def return_client_stats():
    client_stats = get_client_stats()
    if client_stats is None:
        abort(404, 'No client has polled yet')
    return make_response(client_stats)
